"use strict";

const APP_ID = 'amzn1.ask.skill.844c16ea-53ee-4809-92e4-372e5523fde3';

const Alexa = require("alexa-sdk");

exports.handler = function (event, context, callback) {
  const alexa = Alexa.handler(event, context);
  alexa.appId = APP_ID;

  alexa.resources = require('./l18n/languageStrings');

  alexa.registerHandlers(handlers);
  alexa.execute();
};

const handlers = {
  "LaunchRequest": startResponse,
  "AMAZON.CancelIntent": exitResponse,
  "AMAZON.StopIntent": exitResponse,
  "AMAZON.HelpIntent": helpResponse,
  "Unhandled": unhandledResponse,
};

function startResponse() {
  this.emit(":ask", this.t("WELCOME") + this.t("HELP_LIGHT"));
}

function helpResponse() {
  this.emit(":ask", this.t("HELP"), this.t("MORE_HELP"));
}

function exitResponse() {
  this.emit(':tell', this.t("ALL_OVER"));
}

function unhandledResponse() {
  this.emit(':ask', this.t("PROBLEM_UNHANDLED") + this.t("HELP_LIGHT"), this.t("HELP_LIGHT"));
}
