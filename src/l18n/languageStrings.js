'use strict';

/**
 * When editing your questions pay attention to your punctuation. Make sure you use question marks or periods.
 * Make sure the first answer is the correct one. Set at least ANSWER_COUNT answers, any extras will be shuffled in.
 */
module.exports = {
  "en": {
    "translation": {
    }
  },
  "de": {
    "translation": {
      "SKILL_NAME": "Synonymsuche",
      "WELCOME" : '<s>Lass uns anfangen!</s>',

      "PROBLEM_UNHANDLED": "<s>Das habe ich nicht verstanden.</s>",

      "HELP_LIGHT": "<s>Ich kann dir nicht helfen.</s>",
      "HELP" : "<s>Ich kann dir nicht helfen.</s>",
      "MORE_HELP" : "<s>Ich kann dir nicht helfen.</s>",

      "ALL_OVER" : "<s>Die Synonymsuche wird beendet.</s>",
    }
  }
};
